package org.example.transformer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.List;

public class TransformMeetingToHTML {

    public static void generateHTML(List<Node> meetingNodes, String xsltFilePath, String htmlOutputFilePath) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element htmlElement = document.createElement("html");
            document.appendChild(htmlElement);

            Element bodyElement = document.createElement("body");
            htmlElement.appendChild(bodyElement);

            for (Node meetingNode : meetingNodes) {
                Node importedNode = document.importNode(meetingNode, true);
                bodyElement.appendChild(importedNode);
            }

            File temporaryXmlFile = File.createTempFile("tempMeetings", ".xml");

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer xmlTransformer = transformerFactory.newTransformer();
            xmlTransformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource domSource = new DOMSource(document);
            StreamResult temporaryXmlResult = new StreamResult(temporaryXmlFile);
            xmlTransformer.transform(domSource, temporaryXmlResult);

            Transformer xsltTransformer = transformerFactory.newTransformer(new StreamSource(xsltFilePath));
            StreamSource xmlSource = new StreamSource(temporaryXmlFile);
            StreamResult htmlResult = new StreamResult(new File(htmlOutputFilePath));
            xsltTransformer.transform(xmlSource, htmlResult);

            temporaryXmlFile.delete();
            System.out.println("HTML file with schedule was generated : " + htmlOutputFilePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
