package org.example.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MeetingsParser {

    public static List<Node> filterMeetingsByDate(String xmlFilePath, Date specifiedDate) {
        List<Node> filteredMeetings = new ArrayList<>();
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(new File(xmlFilePath));
            NodeList meetingNodeList = document.getElementsByTagName("Meeting");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            for (int i = 0; i < meetingNodeList.getLength(); i++) {
                Node meetingNode = meetingNodeList.item(i);
                if (meetingNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element meetingElement = (Element) meetingNode;
                    Element dateTimeElement = (Element) meetingElement.getElementsByTagName("DateTime").item(0);
                    String dateText = dateTimeElement.getTextContent().substring(0, 10);
                    Date meetingDate = dateFormat.parse(dateText);

                    if (meetingDate.equals(specifiedDate)) {
                        filteredMeetings.add(meetingNode);
                        System.out.println("Meeting found:");
                        System.out.println("Date: " + dateText);
                        System.out.println("Partner: " + meetingElement.getElementsByTagName("Partner").item(0).getTextContent());
                        System.out.println("Meeting venue: " + meetingElement.getElementsByTagName("MeetingVenue").item(0).getTextContent());
                        System.out.println();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filteredMeetings;
    }
}
