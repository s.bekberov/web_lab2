package org.example;

import org.example.parser.MeetingsParser;
import org.example.transformer.TransformMeetingToHTML;
import org.w3c.dom.Node;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("dd-MM-yyyy:");
        String inputDate = scanner.nextLine();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = dateFormat.parse(inputDate);

        List<Node> nodes = MeetingsParser.filterMeetingsByDate("src/main/java/resources/Meetings.xml", date);
        TransformMeetingToHTML.generateHTML(nodes, "src/main/java/resources/Meeting.xsl", "schedule.html");

        scanner.close();
    }
}
