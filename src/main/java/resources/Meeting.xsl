<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>Meetings</title>
                <style>
                    body {
                    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                    background-color: #f4f4f9;
                    color: #333;
                    margin: 0;
                    padding: 20px;
                    }
                    h2 {
                    color: #4CAF50;
                    text-align: center;
                    }
                    table {
                    width: 100%;
                    border-collapse: collapse;
                    margin: 20px 0;
                    font-size: 16px;
                    text-align: left;
                    }
                    th, td {
                    border: 1px solid #ddd;
                    padding: 12px;
                    }
                    th {
                    background-color: #4CAF50;
                    color: white;
                    }
                    tr:nth-child(even) {
                    background-color: #f2f2f2;
                    }
                    tr:hover {
                    background-color: #ddd;
                    }
                </style>
            </head>
            <body>
                <h2>Meetings</h2>
                <table>
                    <tr>
                        <th>Date and Time</th>
                        <th>Partner</th>
                        <th>Meeting venue</th>
                    </tr>
                    <xsl:apply-templates select="node()"/>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="Meeting">
        <tr>
            <td><xsl:value-of select="DateTime"/></td>
            <td><xsl:value-of select="Partner"/></td>
            <td><xsl:value-of select="MeetingVenue"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
